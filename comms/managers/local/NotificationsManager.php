<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Managers\Local;

/**
 * Description of NotificationsManager
 *
 * @author sbanda
 */
class NotificationsManager extends \Ease\Orm\ResourceManager {

    protected function getClassIdentifier() {
        return "id";
    }

    protected function getResourceClassName() {
        return "\Ease\Comms\Notification";
    }

    protected function getTableName() {
        return "Ease__Notifications";
    }

    protected function getClassProperties() {
        return array(
            'id' => array(
                'name' => 'id',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_READ_ONLY,
                )
            ),
            'ownerId' => array(
                'name' => 'ownerId',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_CREATE_ONLY,
                )
            ),
            'subject' => array(
                'name' => 'subject',
            ),
            'content' => array(
                'name' => 'content',
            ),
            'dateCreated' => array(
                'name' => 'dateCreated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_CREATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'isCurrentTimeStamp',
                )
            ),
            'dateViewed' => array(
                'name' => 'dateViewed',
            ),
        );
    }

    protected function getClassAssociations() {
        return array(
            'meta' => array(
                'name' => 'meta',
                'table' => 'Ease__Notifications_MetaData',
                'fields' => array(
                    'dataKey',
                    'dataValue',
                ),
                'criteria' => array(
                    'notificationId' => 'id',
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'writeFormatMetaData',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'readFormatMetaData',
                ),
                'default' => []
            ),
        );
    }

    protected function getClassCollections() {
        return array();
    }

}
