<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Managers\Local;

/**
 * Description of BroadcastsManager
 *
 * @author sbanda
 */
class BroadcastsManager extends \Ease\Orm\ResourceManager {

    protected function getResourceClassName() {
        return "\Ease\Comms\Broadcast";
    }

    protected function getTableName() {
        return "Ease__Broadcasts";
    }

    protected function getClassIdentifier() {
        return "id";
    }

    protected function getClassProperties() {
        return array(
            'id' => array(
                'name' => 'id',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_READ_ONLY,
                ),
            ),
            'name' => array(
                'name' => 'name',
            ),
            'type' => array(
                'name' => 'type',
            ),
            'dueDate' => array(
                'name' => 'dueDate',
            ),
            'dateCreated' => array(
                'name' => 'dateCreated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_CREATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'isCurrentTimeStamp',
                ),
            ),
            'lastUpdated' => array(
                'name' => 'lastUpdated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_UPDATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_UPDATE => 'isCurrentTimeStamp',
                )
            ),
            'status' => array(
                'name' => 'status',
                'default' => \Ease\Comms\Broadcast::STATUS_PENDING,
            ),
        );
    }

    protected function getClassAssociations() {
        return array(
            'meta' => array(
                'name' => 'meta',
                'table' => 'Ease__Broadcasts_MetaData',
                'fields' => array(
                    'dataKey',
                    'dataValue',
                ),
                'criteria' => array(
                    'broadcastId' => 'id',
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'writeFormatMetaData',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'readFormatMetaData',
                ),
                'default' => [],
            ),
            'distributionLists' => array(
                'name' => 'distributionLists',
                'table' => 'Ease__Broadcasts_DistributionLists',
                'fields' => array(
                    'listId',
                ),
                'criteria' => array(
                    'broadcastId' => 'id',
                ),
                'default' => [],
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'inflateAssociationArray',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'deflateAssociationArray',
                )
            )
        );
    }

    protected function getClassCollections() {
        return array(
            'recipients' => array(
                'name' => 'recipients',
                'manager' => "\Ease\Comms\Managers\Local\RecipientsManager",
                'criteria' => array(
                    'broadcastId' => 'id',
                ),
                'default' => [],
            ),
        );
    }
    
}
