<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Managers\Local;

/**
 * Description of RecipientsManager
 *
 * @author sbanda
 */
class RecipientsManager extends \Ease\Orm\ResourceManager {

    protected function getResourceClassName() {
        return "\Ease\Comms\Recipient";
    }

    protected function getTableName() {
        return "Ease__Broadcasts_Recipients";
    }

    protected function getClassIdentifier() {
        return "id";
    }

    protected function getClassProperties() {
        return array(
            'id' => array(
                'name' => 'id',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_READ_ONLY,
                ),
            ),
            'broadcastId' => array(
                'name' => 'broadcastId',
            ),
            'dateCreated' => array(
                'name' => 'dateCreated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_CREATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'isCurrentTimeStamp',
                ),
            ),
            'lastUpdated' => array(
                'name' => 'lastUpdated',
                'rules' => array(
                    \Ease\Orm\Components\ClassComponent::RULE_UPDATE_ONLY,
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_UPDATE => 'isCurrentTimeStamp',
                ),
            )
        );
    }

    protected function getClassAssociations() {
        return array(
            'meta' => array(
                'name' => 'meta',
                'table' => 'Ease__Broadcasts_Recipients_MetaData',
                'fields' => array(
                    'dataKey',
                    'dataValue',
                ),
                'criteria' => array(
                    'recipientId' => 'id',
                ),
                'callbacks' => array(
                    \Ease\Orm\Components\ClassComponent::EVENT_CREATE => 'writeFormatMetaData',
                    \Ease\Orm\Components\ClassComponent::EVENT_RETRIEVE => 'readFormatMetaData',
                ),
                'default' => [],
            ),
        );
    }

    protected function getClassCollections() {
        return array();
    }

}
