<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms;

/**
 * Description of Notification
 *
 * @author sbanda
 */
class Notification extends \Ease\Resources\Resource {
    
    use \Ease\Traits\MetaTraits;
    
    protected $id;
    protected $ownerId;
    protected $subject;
    protected $content;
    protected $dateCreated;
    protected $dateViewed;
    
    public function getId() {
        return $this->id;
    }

    public function getOwnerId() {
        return $this->ownerId;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function getContent() {
        return $this->content;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    public function getDateViewed() {
        return $this->dateViewed;
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function setOwnerId($ownerId) {
        $this->ownerId = (int) $ownerId;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function setDateCreated($dateCreated) {
        $this->dateCreated = (int) $dateCreated;
    }

    public function setDateViewed($dateViewed) {
        $this->dateViewed = (int) $dateViewed;
    }

}
