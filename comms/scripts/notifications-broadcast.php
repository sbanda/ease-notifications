<?php

namespace Ease\Comms\Scripts;

$_SERVER['EASE_ENVIRONMENT'] = 'looklane-local';

require_once '../../../../../bootstrap.php';

$db = \Ease\Utils\DatabaseSettingsManager::getInstance()->getConnection('default');

$broadcastsManager = new \Ease\Comms\Managers\Local\BroadcastsManager($db);

$pendingNotificationBroadcastsCriteria = [
    'type' => \Ease\Comms\Broadcast::TYPE_NOTIFICATION,
    'status' => \Ease\Comms\Broadcast::STATUS_PENDING,
    'dueDate' => array('_lte' => mktime(null,null,59)),
];

error_log("Search for pending broadcast due on or before " . date('Y-m-d H:i:59'));
$pendingNotifications = $broadcastsManager->search($pendingNotificationBroadcastsCriteria);

if (!$pendingNotifications) {
    error_log("No pending broadcasts found, exiting script");
    exit();
}

error_log(count($pendingNotifications) . "pending broadcasts found");
foreach ($pendingNotifications as $pendingBroadcast) {
    error_log("Processing broadcast " . $pendingBroadcast->getName() . " (" . $pendingBroadcast->getId()) . ")";
    $pendingBroadcast->setStatus(\Ease\Comms\Broadcast::STATUS_PROCESSING);
    $broadcastsManager->update($pendingBroadcast);

    $recipientIds = [];

    if ($pendingBroadcast->getMeta('sendToAll') == true) {
        if ($db->select("LookLane__Users", "id")) {
            $recipientIds = $db->fetchAll();
        }
    }

    if ($recipientIds) {
        $usersManager = new \LookLane\Users\Managers\Local\UsersManager($db);
        $notificationsManager = new \Ease\Comms\Managers\Local\NotificationsManager($db);

        foreach ($recipientIds as $userCriteria) {
            $recipient = $usersManager->retrieve($userCriteria);

            $notification = new \Ease\Comms\Notification();
            $notification->setOwnerId($recipient->getId());
            $notification->setSubject($pendingBroadcast->getMeta('subject'));
            $notification->setContent($pendingBroadcast->getMeta('content'));
            $notification->setMetaData('type', $pendingBroadcast->getMeta('type'));
            $notificationsManager->create(($notification));
            
            unset($recipient);
            unset($pendingBroadcast);
        }
    }

    $pendingBroadcast->setStatus(\Ease\Comms\Broadcast::STATUS_PROCESSED);
    $broadcastsManager->update($pendingBroadcast);
    unset($pendingBroadcast);
}