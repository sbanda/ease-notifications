<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms;

/**
 * Description of Recipient
 *
 * @author sbanda
 */
class Recipient extends \Ease\Resources\Resource {
    
    use \Ease\Traits\MetaTraits;
    
    protected $id;
    protected $broadcastId;
    protected $dateCreated;
    protected $lastUpdated;
    
    protected function init() {
        parent::init();
        $this->addInternalProperty('broadcastId');
    }
    
    public function getId() {
        return $this->id;
    }

    public function getBroadcastId() {
        return $this->broadcastId;
    }
    
    public function getDateCreated() {
        return $this->dateCreated;
    }
    
    public function getLastUpdated() {
        return $this->lastUpdated;
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function setBroadcastId($broadcastId) {
        $this->broadcastId = (int) $broadcastId;
    }

    public function setDateCreated($dateCreated) {
        $this->dateCreated = (int) $dateCreated;
    }
    
    public function setLastUpdated($lastUpdated) {
        $this->lastUpdated = (int) $lastUpdated;
    }
}
