<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Resources;

/**
 * Description of BroadcastController
 *
 * @author sbanda
 */
class BroadcastController extends \Ease\Requests\Controllers\ResourceController {
    
    use \Ease\Requests\Controllers\Traits\InputValidation;
    
    protected function init() {
        parent::init();
        $this->response->setHeader('Content-Type', "application/json");
    }

    protected function getResourceManager() {
        $dbm = \Ease\Utils\DatabaseSettingsManager::getInstance();
        $db = $dbm->getConnection('default');
        return new \Ease\Comms\Managers\Local\BroadcastsManager($db);
    }

    public function handleDelete() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        $criteria = array(
            'id' => $this->environment->getParam('id'),
        );
        $broadcast = $this->getResourceManager()->retrieve($criteria);
        $this->getResourceManager()->delete($broadcast);
        
        return $this->response;
    }

    public function handleGet() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        $criteria = array(
            'id' => $this->environment->getParam('id'),
        );
        $broadcast = $this->getResourceManager()->retrieve($criteria);
        
        $this->response->setOutput($broadcast->render());
        
        return $this->response;
    }

    public function handlePost() {
        self::$REQUIRED_FIELDS = array(
            'name',
            'type',
            'dueDate',
        );
        $this->validateInput($this->environment->getRawPostdata());
        
        $broadcast = new \Ease\Comms\Broadcast();
        $broadcast->setName($this->input['name']);
        $broadcast->setType($this->input['type']);
        $broadcast->setDueDate($this->input['dueDate']);
        
        if (array_key_exists('status', $this->input)) {
            $broadcast->setStatus($this->input['status']);
        }
        
        if (array_key_exists('distributionLists', $this->input)) {
            $broadcast->setDistributionLists($this->input['distributionLists']);
        }
        
        if (array_key_exists('meta', $this->input)) {
            $broadcast->setMeta($this->input['meta']);
        }

        $this->getResourceManager()->create($broadcast);
        
        $this->response->setOutput($broadcast->render());
        return $this->response;
    }

    public function handlePut() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        self::$REQUIRED_FIELDS = array(
            'name',
            'type',
            'dueDate',
            'status',
            'distributionLists',
            'meta'
        );
        $this->validateInput($this->environment->getRawPostdata());

        $criteria = array(
            'id' => $this->environment->getParam('id'),
        );
        $broadcast = $this->getResourceManager()->retrieve($criteria);
        
        $broadcast->setName($this->input['name']);
        $broadcast->setType($this->input['type']);
        $broadcast->setDueDate($this->input['dueDate']);
        $broadcast->setStatus($this->input['status']);
        $broadcast->setDistributionLists($this->input['distributionLists']);
        $broadcast->setMeta($this->input['meta']);
        $this->getResourceManager()->update($broadcast);
        
        $this->response->setOutput($broadcast->render());
        return $this->response;
    }

}
