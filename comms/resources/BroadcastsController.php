<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Resources;

/**
 * Description of BroadcastsController
 *
 * @author sbanda
 */
class BroadcastsController extends \Ease\Requests\Controllers\CollectionController {

    protected function getResourceManager() {
        $dbm = \Ease\Utils\DatabaseSettingsManager::getInstance();
        $db = $dbm->getConnection('default');
        return new \Ease\Comms\Managers\Local\BroadcastsManager($db);
    }

}
