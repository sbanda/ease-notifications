<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Resources;

/**
 * Description of RecipientController
 *
 * @author sbanda
 */
class RecipientController extends \Ease\Requests\Controllers\ResourceController {
    
    use \Ease\Requests\Controllers\Traits\InputValidation;
    
    protected function init() {
        parent::init();
        $this->response->setHeader('Content-Type', "application/json");
    }

    protected function getResourceManager() {
        $dbm = \Ease\Utils\DatabaseSettingsManager::getInstance();
        $db = $dbm->getConnection('default');
        return new \Ease\Comms\Managers\Local\RecipientsManager($db);
    }

    public function handleDelete() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        $criteria = array(
            'id' => $this->environment->getParam('id'),
        );
        $recipient = $this->getResourceManager()->retrieve($criteria);
        $this->getResourceManager()->delete($recipient);
        
        return $this->response;
    }

    public function handleGet() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        $criteria = array(
            'id' => $this->environment->getParam('id'),
        );
        $recipient = $this->getResourceManager()->retrieve($criteria);
        
        $this->response->setOutput($recipient->render());
        
        return $this->response;
    }

    public function handlePost() {
        self::$REQUIRED_FIELDS = array(
            'broadcastId',
            'meta',
        );
        
        $this->validateInput($this->environment->getRawPostdata());
        
        $recipient = new \Ease\Comms\Recipient();
        $recipient->setBroadcastId($this->input['broadcastId']);
        $recipient->setMeta($this->input['meta']);
        $this->getResourceManager()->create($recipient);

        $this->response->setOutput($recipient->render());
        return $this->response;
    }

    public function handlePut() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        self::$REQUIRED_FIELDS = array(
            'meta',
        );
        $this->validateInput($this->environment->getRawPostdata());
        
        $criteria = array(
            'id' => $this->environment->getParam('id'),
        );
        $recipient = $this->getResourceManager()->retrieve($criteria);
        $recipient->setMeta($this->input['meta']);
        $this->getResourceManager()->update($recipient);
        
        $this->response->setOutput($recipient->render());
        return $this->response;
    }

}
