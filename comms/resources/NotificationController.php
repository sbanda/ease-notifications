<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms\Resources;

/**
 * Description of NotificationController
 *
 * @author sbanda
 */
class NotificationController extends \Ease\Requests\Controllers\ResourceController {
    
    use \Ease\Requests\Controllers\Traits\InputValidation;
    
    protected function init() {
        parent::init();
        $this->response->setHeader('Content-Type', "application/json");
    }

    protected function getResourceManager() {
        $dbm = \Ease\Utils\DatabaseSettingsManager::getInstance();
        $db = $dbm->getConnection('default');
        return new \Ease\Comms\Managers\Local\NotificationsManager($db);
    }

    public function handleDelete() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        $criteria = ['id' => $this->environment->getParam('id')];
        $notification = $this->getResourceManager()->retrieve($criteria);
        $this->getResourceManager()->delete($notification);
        
        return $this->response;
    }

    public function handleGet() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        $criteria = ['id' => $this->environment->getParam('id')];
        $notification = $this->getResourceManager()->retrieve($criteria);
        
        $this->response->setOutput($notification->render());
        return $this->response;
    }

    public function handlePost() {
        self::$REQUIRED_FIELDS = ['ownerId', 'subject', 'content'];
        $this->validateInput($this->environment->getRawPostdata());
        
        $notification = new \Ease\Comms\Notification();
        $notification->setOwnerId($this->input['ownerId']);
        $notification->setSubject($this->input['subject']);
        $notification->setContent($this->input['content']);
        
        if ($this->input['meta']) {
            $notification->setMeta($this->input['meta']);
        }
        
        $this->getResourceManager()->create($notification);
        
        $this->response->setOutput($notification->render());
        return $this->response;
    }

    public function handlePut() {
        if (!$this->environment->getParam('id')) {
            throw new \Exception("Please specify an ID");
        }
        
        self::$REQUIRED_FIELDS = ['subject', 'content'];
        $this->validateInput($this->environment->getRawPostdata());
        
        $criteria = ['id' => $this->environment->getParam('id')];
        $notification = $this->getResourceManager()->retrieve($criteria);
        
        $notification->setSubject($this->input['subject']);
        $notification->setContent($this->input['content']);        
        $notification->setMeta($this->input['meta']);
        
        $this->getResourceManager()->update($notification);
        
        $this->response->setOutput($notification->render());
        return $this->response;
    }

}
