<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ease\Comms;

/**
 * Description of Broadcast
 *
 * @author sbanda
 */
class Broadcast extends \Ease\Resources\Resource {
    
    const STATUS_PENDING = "PENDING";
    const STATUS_PROCESSING = "PROCESSING";
    const STATUS_PROCESSED = "PROCESSED";
    const STATUS_CANCELLED = "CANCELLED";
    
    const TYPE_NOTIFICATION = "NOTIFICATION";
    const TYPE_BULLETIN = "BULLETIN";
    const TYPE_EMAIL = "EMAIL";
    const TYPE_SMS = "SMS";
    
    use \Ease\Traits\MetaTraits;
    
    protected $id;
    protected $name;
    protected $type;
    protected $dueDate;
    protected $dateCreated;
    protected $lastUpdated;
    protected $status;
    protected $distributionLists;
    protected $segments;
    protected $recipients;
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getType() {
        return $this->type;
    }

    public function getDueDate() {
        return $this->dueDate;
    }

    public function getDateCreated() {
        return $this->dateCreated;
    }

    public function getLastUpdated() {
        return $this->lastUpdated;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getDistributionLists() {
        return $this->distributionLists;
    }

    public function getSegments() {
        return $this->segments;
    }

    public function getRecipients() {
        return $this->recipients;
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setType($type) {
        switch ($type) {
            case self::TYPE_BULLETIN:
            case self::TYPE_EMAIL:
            case self::TYPE_NOTIFICATION:
            case self::TYPE_SMS:
                $this->type = $type;
                break;
            default:
                throw new \Exception("Unsupported broadcast type: $type");
        }
    }

    public function setDueDate($dueDate) {
        $this->dueDate = (int) $dueDate;
    }

    public function setDateCreated($dateCreated) {
        $this->dateCreated = (int) $dateCreated;
    }

    public function setLastUpdated($lastUpdated) {
        $this->lastUpdated = (int) $lastUpdated;
    }

    public function setStatus($status) {
        switch ($status) {
            case self::STATUS_PENDING:
            case self::STATUS_PROCESSING:
            case self::STATUS_PROCESSED:
            case self::STATUS_CANCELLED:
                $this->status = $status;
                break;
            default :
                $this->status = self::STATUS_CANCELLED;
                break;
        }
    }

    public function setDistributionLists(array $distributionLists) {
        $this->distributionLists = $distributionLists;
    }

    public function setSegments(array $segments) {
        $this->segments = $segments;
    }

    public function setRecipients(array $recipients) {
        $this->recipients = $recipients;
    }
    
    public function export() {
        $broadcast = parent::export();
        
        $recipients = array();
        if ($this->getRecipients()) {
            foreach ($this->getRecipients() as $recipient) {
                $recipients[] = $recipient->export();
            }

            $broadcast['recipients'] = $recipients;
        }
        
        return $broadcast;
    }

}
